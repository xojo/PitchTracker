#tag Class
Protected Class Pitch
	#tag Property, Flags = &h0
		Action As Actions = Actions.NotSpecified
	#tag EndProperty

	#tag Property, Flags = &h0
		Location As Xojo.Core.Point
	#tag EndProperty


	#tag Enum, Name = Actions, Type = Integer, Flags = &h0
		Ball
		  Strike
		  InPlay
		NotSpecified
	#tag EndEnum


	#tag ViewBehavior
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			InitialValue="-2147483648"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Action"
			Group="Behavior"
			InitialValue="Actions.NotSpecified"
			Type="Actions"
		#tag EndViewProperty
	#tag EndViewBehavior
End Class
#tag EndClass
