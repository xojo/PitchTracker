#tag IOSView
Begin iosView PitchView
   BackButtonTitle =   ""
   Compatibility   =   ""
   Left            =   0
   NavigationBarVisible=   True
   TabIcon         =   ""
   TabTitle        =   ""
   Title           =   "Pitch Tracker"
   Top             =   0
   Begin iOSCanvas PitchCanvas
      AccessibilityHint=   ""
      AccessibilityLabel=   ""
      AutoLayout      =   PitchCanvas, 4, BottomLayoutGuide, 4, False, +1.00, 4, 1, 0, , True
      AutoLayout      =   PitchCanvas, 1, <Parent>, 1, False, +1.00, 4, 1, 0, , True
      AutoLayout      =   PitchCanvas, 2, <Parent>, 2, False, +1.00, 4, 1, 0, , True
      AutoLayout      =   PitchCanvas, 3, TopLayoutGuide, 4, False, +1.00, 4, 1, 0, , True
      Height          =   415.0
      Left            =   0
      LockedInPosition=   False
      Scope           =   0
      Top             =   65
      Visible         =   True
      Width           =   320.0
   End
   Begin iOSToolButton ResetButton
      Caption         =   "Reset"
      Enabled         =   True
      Height          =   22
      Image           =   "0"
      Image           =   "0"
      Left            =   8
      LockedInPosition=   False
      Scope           =   0
      Top             =   32
      Type            =   "1001"
      Width           =   45.0
   End
   Begin iOSToolButton BallButton
      Caption         =   "Ball"
      Enabled         =   True
      Height          =   22
      Image           =   "0"
      Image           =   "0"
      Left            =   8
      LockedInPosition=   False
      Scope           =   0
      Top             =   446
      Type            =   "1001"
      Width           =   32.0
   End
   Begin iOSToolButton StrikeButton
      Caption         =   "Strike"
      Enabled         =   True
      Height          =   22
      Image           =   "0"
      Image           =   "0"
      Left            =   49
      LockedInPosition=   False
      Scope           =   0
      Top             =   446
      Type            =   "1001"
      Width           =   45.0
   End
   Begin iOSToolButton InPlayButton
      Caption         =   "In Play"
      Enabled         =   True
      Height          =   22
      Image           =   "0"
      Image           =   "0"
      Left            =   103
      LockedInPosition=   False
      Scope           =   0
      Top             =   446
      Type            =   "1001"
      Width           =   50.0
   End
End
#tag EndIOSView

#tag WindowCode
	#tag Event
		Sub ToolbarPressed(button As iOSToolButton)
		  Select Case button
		  Case ResetButton
		    // Clear pitches
		    Redim Pitches(-1)
		  Case BallButton
		    If Pitches.Ubound >= 0 Then
		      Pitches(Pitches.Ubound).Action = Pitch.Actions.Ball
		      
		    End If
		  Case StrikeButton
		    If Pitches.Ubound >= 0 Then
		      Pitches(Pitches.Ubound).Action = Pitch.Actions.Strike
		    End If
		  Case InPlayButton
		    If Pitches.Ubound >= 0 Then
		      Pitches(Pitches.Ubound).Action = Pitch.Actions.InPlay
		    End If
		  End Select
		  
		  // Redraw canvas
		  PitchCanvas.Invalidate
		  
		End Sub
	#tag EndEvent


	#tag Property, Flags = &h0
		Pitches() As Pitch
	#tag EndProperty


#tag EndWindowCode

#tag Events PitchCanvas
	#tag Event
		Sub Paint(g As iOSGraphics)
		  // Background is all black
		  g.FillColor = &c00000000
		  g.FillRect(0, 0, g.Width, g.Height)
		  
		  // The pitch grid is in the center
		  Dim horizontalMargin As Double = g.Width * 0.30
		  Dim verticalMargin As Double = g.Height * 0.30
		  
		  // Draw the outline for the pitch grid
		  g.LineColor = &cCCCCCC00
		  g.LineWidth = 4
		  g.DrawRect(horizontalMargin, verticalMargin, g.Width - horizontalMargin * 2, g.Height - verticalMargin * 2)
		  
		  // Draw the horizontal lines of the pitch grid
		  g.LineColor = &c99999900
		  Dim hSize As Double = (g.Height - verticalMargin * 2) / 3
		  For i As Integer = 1 To 2
		    g.DrawLine(horizontalMargin, verticalMargin + hSize * i, g.Width - horizontalMargin, verticalMargin + hSize * i)
		  Next
		  
		  // Draw the vertical lines of the pitch grid
		  Dim wSize As Double = (g.Width - horizontalMargin * 2) / 3
		  For i As Integer = 1 To 2
		    g.DrawLine(horizontalMargin + wSize * i, verticalMargin, horizontalMargin + wSize * i, g.Height - verticalMargin)
		  Next
		  
		  // Draw the pitch locations
		  Dim ballSize As Double = hSize / 2
		  Const kOffset As Double = 8
		  For i As Integer = 0 To Pitches.Ubound
		    // Draw the ball at the tapped location
		    Dim pitchColor As Color
		    Select Case Pitches(i).Action
		    Case Pitch.Actions.Ball
		      g.FillColor = &c00ff0000
		    Case Pitch.Actions.Strike
		      g.FillColor = &cff000000
		    Case Pitch.Actions.InPlay
		      g.FillColor = &c0000ff00
		    Case Else
		      g.FillColor = &cFFFFFF00
		    End Select
		    
		    Dim centerX As Double = Pitches(i).Location.X - ballSize / 2
		    Dim centerY As Double = Pitches(i).Location.Y - ballSize / 2
		    
		    g.FillOval(centerX, centerY, ballSize, ballSize)
		    
		    // Draw the pitch number within the ball
		    g.FillColor = &c00000000
		    
		    Dim font As iOSFont = iOSFont.SystemFont(30)
		    g.TextFont = font
		    Dim pitchCount As Integer = i + 1
		    g.DrawTextLine(pitchCount.ToText, Pitches(i).Location.X - kOffset, Pitches(i).Location.Y + kOffset, -1, iOSTextAlignment.Center)
		  Next
		End Sub
	#tag EndEvent
	#tag Event
		Sub PointerUp(pos As Xojo.Core.Point, eventInfo As iOSEventInfo)
		  // A tap adds a pitch location
		  
		  Dim p As New Pitch
		  p.Location = pos
		  
		  Pitches.Append(p)
		  
		  // Redraw canvas
		  Me.Invalidate
		End Sub
	#tag EndEvent
#tag EndEvents
#tag ViewBehavior
	#tag ViewProperty
		Name="BackButtonTitle"
		Group="Behavior"
		Type="Text"
		EditorType="MultiLineEditor"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Index"
		Visible=true
		Group="ID"
		InitialValue="-2147483648"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Left"
		Visible=true
		Group="Position"
		InitialValue="0"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Name"
		Visible=true
		Group="ID"
		Type="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="NavigationBarVisible"
		Group="Behavior"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Super"
		Visible=true
		Group="ID"
		Type="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="TabIcon"
		Group="Behavior"
		Type="iOSImage"
	#tag EndViewProperty
	#tag ViewProperty
		Name="TabTitle"
		Group="Behavior"
		Type="Text"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Title"
		Group="Behavior"
		Type="Text"
		EditorType="MultiLineEditor"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Top"
		Visible=true
		Group="Position"
		InitialValue="0"
		Type="Integer"
	#tag EndViewProperty
#tag EndViewBehavior
